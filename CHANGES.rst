=========
Changelog
=========

1.0.5 (2013-08-30)
==================

- Fixes a typo with the variable MMC which should be MCC,

1.0.4 (2013-08-27)
==================

- Added a handler attribute to the message received function so that when a message is received we can know which
  handler to use to process the message.

1.0.3 (2013-08-27)
==================

- Updated the SMPP lib to be able to talk to Dialogue SMPP servers.

1.0.0 (2013-08-15)
==================

- Initial checkin. Base project gotten from https://github.com/podshumok/python-smpplib