import logging

import smpplib.gsm
import smpplib.client
import smpplib.consts
import smpplib.pdu

# if you want to know what's happening
logging.basicConfig(level='DEBUG')


def received_handler(**kwargs):
    print 'MESSAGE RECEIVED: ', kwargs

    pdu = kwargs['pdu']
    print 'Is error: ', pdu.is_error()
    print 'To: ', pdu.destination_addr
    print 'From: ', pdu.source_addr
    print 'Message: ', pdu.short_message


def sent_handler(**kwargs):
    print 'MESSAGE SENT: ', kwargs
    print kwargs['pdu']


client = smpplib.client.Client('smpp.uk.dialogue.net', 8101)
client.set_message_received_handler(received_handler)
client.set_message_sent_handler(sent_handler)
client.connect()
client.bind_transceiver(system_id='sa.apitrial6', password='AeY6ouco')

parts, encoding_flag, msg_type_flag = smpplib.gsm.make_parts('Testing the service')

for part in parts:
    pdu = client.send_message(
        source_addr='44598',
        destination_addr='27813821298',
        short_message=part,
        data_coding=encoding_flag,
        esm_class=msg_type_flag,
        registered_delivery=True,
        x_e3_session_id='3456876342',
    )
    print 'PDU Seq: ', pdu.sequence

client.listen()